From 671386acfd382e21b26c35a7a79993363387290a Mon Sep 17 00:00:00 2001
From: Santokh Mehmi <santokh.mehmi@cloudandheat.com>
Date: Tue, 25 Feb 2025 11:17:42 +0100
Subject: [PATCH] Adding backing-up as allowed state depending on config flag

Openstack doesn't allow Instances with Volumes that are in backing-up state to live-migrate.
With Volume Drivers like e.g NetApp, live-migration should be possible because at first a
NetApp Snapshot  is created before the backup started.

Adding a config flag backing_up_live_migration: {True,False} to the /etc/cinder/cinder.conf
gives the possibility to add backing-up as expected state for live-migrations.
This config flag should be used with caution because not all drivers might be able
to behave like that.

Change-Id: I407bf95a0ee5f6738a1351e5bb4f698211a424ff
---
 cinder/opts.py       |  1 +
 cinder/volume/api.py | 25 +++++++++++++++++++++----
 2 files changed, 22 insertions(+), 4 deletions(-)

diff --git a/cinder/opts.py b/cinder/opts.py
index 02747fa70..7c7e56913 100644
--- a/cinder/opts.py
+++ b/cinder/opts.py
@@ -285,6 +285,7 @@ def list_opts():
                 [cinder_volume_api.volume_host_opt],
                 [cinder_volume_api.volume_same_az_opt],
                 [cinder_volume_api.az_cache_time_opt],
+                [cinder_volume_api.backingup_lm_opt],
                 cinder_volume_driver.volume_opts,
                 cinder_volume_driver.iser_opts,
                 cinder_volume_driver.nvmeof_opts,
diff --git a/cinder/volume/api.py b/cinder/volume/api.py
index 0e20baff0..5fe5b1c7b 100644
--- a/cinder/volume/api.py
+++ b/cinder/volume/api.py
@@ -87,12 +87,17 @@ az_cache_time_opt = cfg.IntOpt('az_cache_duration',
                                help='Cache volume availability zones in '
                                     'memory for the provided duration in '
                                     'seconds')
+backingup_lm_opt = cfg.BoolOpt('backing_up_live_migration',
+                               default=False,
+                               help='Enables instances with backing-up '
+                                    'volumes to livemigrate')
 
 CONF = cfg.CONF
 CONF.register_opt(allow_force_upload_opt)
 CONF.register_opt(volume_host_opt)
 CONF.register_opt(volume_same_az_opt)
 CONF.register_opt(az_cache_time_opt)
+CONF.register_opt(backingup_lm_opt)
 
 CONF.import_opt('glance_core_properties', 'cinder.image.glance')
 
@@ -2431,10 +2436,22 @@ class API(base.Base):
         # locally on the Cinder node.  Just come up with an easy way to
         # determine if we're attaching to the Cinder host for some work or if
         # we're being used by the outside world.
-        expected = {'multiattach': vref.multiattach,
-                    'status': (('available', 'in-use', 'downloading')
-                               if vref.multiattach
-                               else ('available', 'downloading'))}
+
+        # Checking if the backing_up_live_migration config is set True
+        # Only should be enabled if  e.g. NetAppDriver is enabled
+
+        backing_vol_migration_enabled = CONF.backing_up_live_migration
+        expected = {
+            'multiattach': vref.multiattach,
+            'status': (
+                ('available', 'in-use', 'downloading')
+                if vref.multiattach
+                else ('available', 'downloading')
+            )
+        }
+        if backing_vol_migration_enabled:
+            expected['status'] = tuple(
+                list(expected['status']) + ['backing-up'])
 
         result = vref.conditional_update({'status': 'reserved'}, expected)
 
-- 
2.48.1

