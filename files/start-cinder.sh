#!/bin/bash

set -euo pipefail

groupadd --gid ${CINDER_GID} cinder && adduser --system --uid ${CINDER_UID} --gid ${CINDER_GID} cinder

exec setpriv --reuid ${CINDER_UID} --regid ${CINDER_GID} --groups ${CINDER_GID} -- "$@"
