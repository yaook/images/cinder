from oslo_config import cfg
from cinder import version
from cinder import context
from cinder import objects
from typing import List
from keystoneauth1 import loading
from keystoneauth1 import session
from cinderclient.client import Client
import traceback
import platform
import time
import sys

CONF = cfg.CONF


def _check_active_backups(ctxt) -> List:
    backups_creating = objects.BackupList.get_all(ctxt, filters={
        "status": "creating", "host": platform.node()})
    backups_deleting = objects.BackupList.get_all(ctxt, filters={
        "status": "deleting", "host": platform.node()})
    print(f"Found {len(backups_creating)} in state creating.")
    print(f"Found {len(backups_deleting)} in state deleting.")
    return not (len(backups_creating) + len(backups_deleting)) == 0


def wait_and_enable(client):
    while True:
        services = client.services.list(host=platform.node(),
                                       binary='cinder-backup')
        if len(services) > 0 and services[0].state == 'up':
            break
        print("Backup Service on this host is still in state down, \
              will retry in 10s.")
        time.sleep(10)
    print("Service is in state up, going to enable the service.")
    client.services.enable(host=platform.node(), binary='cinder-backup')


def disable_and_wait(client):
    print("Disable Cinder-Backup on this host")
    client.services.disable(host=platform.node(), binary='cinder-backup')
    ctxt = context.get_admin_context()
    while True:
        if not _check_active_backups(ctxt):
            print("No active Backups running on this host, terminating.")
            break

        print("Still active backups running. Retry in 60s")
        time.sleep(60)


def main() -> int:
    objects.register_all()
    CONF([], project='cinder', version=version.version_string())
    CONF.register_opts(
        [
            cfg.StrOpt('auth_url'),
            cfg.StrOpt('username'),
            cfg.StrOpt('password'),
            cfg.StrOpt('project_name'),
            cfg.StrOpt('user_domain_name'),
            cfg.StrOpt('project_domain_name'),
            cfg.StrOpt('valid_interfaces')
        ], cfg.OptGroup('keystone_authtoken'))
    loader = loading.get_plugin_loader('password')
    auth = loader.load_from_options(
        auth_url=CONF.keystone_authtoken.auth_url,
        username=CONF.keystone_authtoken.username,
        password=CONF.keystone_authtoken.password,
        project_name=CONF.keystone_authtoken.project_name,
        user_domain_name=CONF.keystone_authtoken.user_domain_name,
        project_domain_name=CONF.keystone_authtoken.project_domain_name)
    sess = session.Session(auth=auth)
    client = Client('3', session=sess, 
                    endpoint_type=CONF.keystone_authtoken.valid_interfaces,
                    region_name=CONF.keystone_authtoken.region_name)
    retry_counter = 0
    while True:
        try:
            if sys.argv[1] == "enable":
                wait_and_enable(client)
                return 0
            elif sys.argv[1] == "disable":
                disable_and_wait(client)
                return 0
            else:
                print("Missing argument. Possible: {enable, disable}")
                return 1
        except Exception as e:
            retry_counter += 1
            if retry_counter >= 10:
                print("Failure during execution, retry counter exeeded.")
                return 1
            print("Failure during execution, retry in 10s: %s" % e)
            time.sleep(10)


if __name__ == '__main__':
    sys.exit(main())
