##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM python:3.8-slim-bullseye

ARG openstack_release=2023.1
ARG branch=stable/${openstack_release}

ENV CINDER_UID=304
ENV CINDER_GID=304

COPY files/ /build_files
COPY files/policies_${openstack_release}.yaml /default_policy/policy.yaml

RUN set -eux ; \
    export LANG=C.UTF-8 ; \
    #
    BUILD_PACKAGES="git pkg-config build-essential libxml2-dev libxslt1-dev libffi-dev zlib1g-dev libssl-dev librados-dev librbd-dev wget python3-dev libmariadb-dev" ; \
    REQUIRED_PACKAGES="libffi7 lvm2 nfs-common sudo curl procps librados2 librbd1 libglib2.0-0 cryptsetup libmariadb3" ; \
    #
    apt-get update ; \
    apt-get install -y --no-install-recommends ${BUILD_PACKAGES} ${REQUIRED_PACKAGES} ; \
    #
    pip install -U pip ; \
    pip install 'Cython<3.0.0' ; \
    #
    _radosPackageVersion=$(dpkg-query --showformat='${Version}' --show librados2) ; \
    _cephVersion="${_radosPackageVersion%-*}" ; \
    git clone --branch v${_cephVersion} --depth 1 https://github.com/ceph/ceph ; \
    ( cd /ceph/src/pybind/rados ; python setup.py build install ) ; \
    ( cd /ceph/src/pybind/rbd ; python setup.py build install ) ; \
    # Install ceph tool (without building ceph)
    ( cp /ceph/src/pybind/*.py /usr/local/bin ) ; \
    ( cd /ceph; sed "s#@PYTHON_EXECUTABLE@#/usr/local/bin/python3#;s#@CEPH_GIT_NICE_VER@#${_cephVersion}#;s#@CEPH_GIT_VER@#$(git rev-parse HEAD)#;s#@CEPH_RELEASE@#$(sed -n 1p /ceph/src/ceph_release)#;s#@CEPH_RELEASE_NAME@#$(sed -n 2p /ceph/src/ceph_release)#;s#@CEPH_RELEASE_TYPE@#$(sed -n 3p /ceph/src/ceph_release)#" /ceph/src/ceph.in > /usr/local/bin/ceph ) ; \
    ( chmod 0755 /usr/local/bin/ceph ) ; \
    #
    (git clone https://opendev.org/openstack/requirements.git --depth 1 --branch "${branch}" \
      || git clone https://opendev.org/openstack/requirements.git --depth 1 --branch "${openstack_release}-eol" \
      || git clone https://opendev.org/openstack/requirements.git --depth 1 --branch "unmaintained/${openstack_release}"); \
    (git clone https://opendev.org/openstack/cinder.git --depth 1 --branch "${branch}" \
      || git clone https://opendev.org/openstack/cinder.git --depth 1 --branch "${openstack_release}-eol" \
      || git clone https://opendev.org/openstack/cinder.git --depth 1 --branch "unmaintained/${openstack_release}"); \
    #
    pip install -c requirements/upper-constraints.txt PyMySQL mysqlclient python-memcached pymemcache cinder/ ; \
    (pip install git+https://opendev.org/openstack/python-cinderclient.git@${branch} \
      || pip install git+https://opendev.org/openstack/python-cinderclient.git@${openstack_release}-eol \
      || pip install git+https://opendev.org/openstack/python-cinderclient.git@unmaintained/${openstack_release}); \
    #
    # Fix possible race condition in cinder db cleanup https://bugs.launchpad.net/cinder/+bug/2060201
    patch -d /usr/local/lib/python3.*/site-packages/cinder -p 2 -i /build_files/cinder-915064.patch ; \
    #
    # faster volume upload to glance
    cd /usr/local/lib/python3.*/site-packages/glanceclient && patch -p 2 -i /build_files/glanceclient-733984.patch; \
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p 2 -i /build_files/cinder-888676_zed.patch; \
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p 2 -i /build_files/908425.patch; \
    #
    # netapp driver online resize
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p 2 -i /build_files/netapp-volume-online-resize.patch; \
    #
    cd /usr/local/lib/python3.*/site-packages/oslo_privsep && patch -p 2 -i /build_files/privsep-873513.patch; \
    #
    # cinder eventlet tpool fix
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p 2 -i /build_files/tpool.patch; \
    #
    # patch cinder to use --shrink flag on qemu-img resize
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p2 -i /build_files/cinder-922829.patch; \
    #
    # only abort if actual size is greater than expected
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p2 -i /build_files/cinder-925876.patch; \
    #
    # Change scheduler logic for backup service selection during backup creation from random to least busy backup service. 
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p2 -i /build_files/scheduler-fix.patch; \
    #
    # Add backing_up_live_migration config flag
    cd /usr/local/lib/python3.*/site-packages/cinder && patch -p2 -i /build_files/add_backing_up_to_expected.patch; \
    #
    cp /build_files/sudoers /etc/sudoers ; \
    chmod 440 /etc/sudoers ; \
    cp /build_files/start-cinder.sh /start-cinder.sh ; \
    cp /build_files/backup_lifecycle.py /opt/backup_lifecycle.py ; \
    mkdir -p /usr/share/cinder ; \
    mkdir -p /var/lib/cinder/groups ; \
    ln -s /usr/local/etc/cinder/rootwrap.d /usr/share/cinder/rootwrap ; \
    sed -i 's|qemu-img: EnvFilter, env, root, LC_ALL=C, qemu-img|qemu-img: EnvFilter, env, cinder, LC_ALL=C, qemu-img|' /usr/share/cinder/rootwrap/volume.filters ; \
    groupadd --gid ${CINDER_GID} cinder ; \
    adduser --system --uid ${CINDER_UID} --gid ${CINDER_GID} cinder ; \
    chown -R ${CINDER_UID}:${CINDER_GID} /var/lib/cinder ; \
    # Install ceph-common
    # This will install python3-minimal but this will not be a problem
    # for the openstack parts, as these tools will still use the python3
    # installation in /usr/local/bin (which is first in the search path).
    apt install -y --no-install-recommends ceph-common qemu-utils; \
    #
    # Fix certificate validation issues occuring in some k8s clusters in
    # the policy validation job.
    pip3 install --upgrade 'requests[security]' ; \
    #
    # Ensure oslo.messaging>=14.7.0
    echo "oslo.messaging>=$(pip show oslo.messaging | grep Version | awk '{print $2}')" > oslo_messaging_constraint.txt ; \
    pip install -c oslo_messaging_constraint.txt --upgrade oslo.messaging==14.7.0 || true ; \
    #
    cd /usr/local/lib/python3.*/site-packages/oslo_messaging && patch -p2 -i /build_files/fix-rabbitmq-exchange-declare-fallback.patch; \
    #
    # cleanup BUILD environment
    #
    pip uninstall -y Cython ; \
    rm -rf /build_files ; \
    rm -rf requirements cinder ; \
    rm -rf /ceph-${_cephVersion} ; \
    rm -rf /var/lib/apt/lists/* ; \
    apt-get purge --auto-remove -y ${BUILD_PACKAGES} ; \
    rm -rf /root/.cache;\
    # Create symlink for pkill as the operator expects it during a release upgrade
    ln -s /usr/bin/pkill /bin/pkill

USER cinder

CMD ["/start-cinder.sh"]
